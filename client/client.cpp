#include "client.h"

//函数声明
void GetFileNameFromUrl(s8* URL, s8*fullpath);
s64 IsFileExist(s8* filepath);
s32 CurlDownload(const s8* url, const s8* filepath, s64 OffSet);
/*====================================================================
模块名：	构造函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
FTcInstance::FTcInstance() {
	m_dwServerNode = 0;							 //服务器Node号
	m_dwSevInstancId = 0;						//服务器INS的ID
	m_IsClientConn = FALSE;						//客户端状态
	//memset(m_filename,0,sizeof(m_filename));	//初始化
}


/*====================================================================
模块名：	析构函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
FTcInstance::~FTcInstance() {

}

void FTcInstance::DaemonInstanceEntry(CMessage *const pMsg, CApp *pcApp)
{
	if(pMsg->content != NULL)
		printf("DAEMON %s\n",pMsg->content);
	if( NULL == pMsg || NULL == pcApp )
    {
        return;
    }

    u16 wEvent = pMsg->event;                        //接收事件类型

    switch( wEvent )                                 //判断事件类型，进行相应处理
    {
    default:
        break;
    }	
}

/*====================================================================
模块名：	实例入口函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTcInstance::InstanceEntry(CMessage *const pMsg)
{
	//if(pMsg->content != NULL)
		//printf("%s\n",pMsg->content);
	u16 wCurEvent = pMsg->event;
	switch (wCurEvent)
	{
	case FT_EVENT_C2S:				//客户端请求连接
		ClientInitRequest(pMsg);
		NextState(RUNNING_STATE);
		break;
	case FT_EVENT_C_PRCSS_RPLY_C2S:
		ClientProcessSevReply(pMsg);	//客户端处理服务器的回复
		break;
	case FT_EVENT_C_INQUIRE_LIST:				//客户端查询可下载文件
		ClientQuery(pMsg);
		break;
	case FT_EVENT_C_INQUIRE_ACK_RCV:				//客户端处理服务器关于查询的回复
		ClientProcessQueryReply(pMsg);
		break;
	case FT_EVENT_C_REQ_URL:				//客户端请求文件URL
		ClientReqURL(pMsg);
		break;
	case FT_EVENT_C_PRCSS_RPLY_URL:				//客户端处理请求URL回复
		ClientProcessUrlReply(pMsg);
		break;
	case FT_EVENT_C_DOWNLOAD:
		ClientDownload(pMsg);
		break;
	case FT_EVENT_C_TEST:
		ClientTest(pMsg);
		break;
	case OSP_DISCONNECT:				//	osp	断开链接
		ClientDisconn(pMsg);
		NextState(IDLE_STATE);
	default:
		OspPrintf(TRUE, FALSE, "\n错误, 非法事件类型！\n");
		break;
	}
}

// de_bug 函数
void FTcInstance::ClientTest(CMessage *const pMsg)
{
	printf("NOTE:	This is a debug massage\n");
	printf("pMsg->content is %s \n",pMsg->content);
}

/*====================================================================
模块名：	客户端初始化以及请求连接函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTcInstance::ClientInitRequest(CMessage *const pMsg)
{
	if(m_IsClientConn == FALSE)
	{
		//从IP和端口组成的字符串中分离出IP和端口
		s8 SevIP[15];
		u16 SevPort;
		GetIpPort(pMsg->content,SevIP,&SevPort);
		m_dwServerNode = OspConnectTcpNode(inet_addr(SevIP), SevPort, DisCon_Detection_Timeout, DisConn_Detction_Num, 0, NULL);
	
	//de_bug
		//printf("m_dwServerNode = %d \n",m_dwServerNode);
	
		if (m_dwServerNode == INVALID_NODE)
		{
			::OspPrintf(TRUE, FALSE, "Failed to Connect to the Server !\n");
			return ;
		}
	//设置结点断链检测函数
		if (!::OspSetHBParam(m_dwServerNode, DisCon_Detection_Timeout, DisConn_Detction_Num))
		{
			::OspPrintf(TRUE, FALSE, "Error,failed to Set OspSetHBParam!\n");
		}
	//设置结点断链通知函数
		if (::OspNodeDiscCBRegQ(m_dwServerNode, GetAppID(), FT_CLIENT_INS_NUM) != OSP_OK)
		{
			::OspPrintf(TRUE, FALSE, "Error,failed to Set OspNodeDiscCBRegQ!\n");
		}
	//向服务器发送请求
		::OspPost(MAKEIID(FT_SERVER_APP_ID, CInstance::PENDING), FT_EVENT_S_PRCSS_REQ, NULL, 0, m_dwServerNode,MAKEIID(GetAppID(), GetInsID()));
		//de_bug
		//printf("A cmd has send to Server\n");
	}
	
}

/*====================================================================
模块名：	客户端处理服务器回复请求连接的函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTcInstance::ClientProcessSevReply(CMessage *const pMsg)
{
	//de_bug
	//printf("pMsg->content is = %s \n",pMsg->content);
	TMsg T_msg_req;
	memset(&T_msg_req,0,sizeof(T_msg_req));
	memcpy(&T_msg_req,(TMsg*)pMsg->content,sizeof(T_msg_req));
	if (T_msg_req.m_flag == FALSE)
	{
		//de_bug
			printf("server feject connect,%s, %d \n",T_msg_req.m_msg,T_msg_req.m_flag);
		
		::OspPrintf(TRUE, FALSE, "%s\n", T_msg_req.m_msg);  //打印拒绝连接消息
		::OspPost(MAKEIID(GetAppID(), GetInsID()), OSP_DISCONNECT, NULL, 0); //断开osp
	}
	else
	{
		//de_bug
			//printf("server  connected, %s, %d \n",T_msg_req.m_msg,T_msg_req.m_flag);
	printf("server  connected!\n");
		m_dwSevInstancId = pMsg->srcid;  //保存服务器实例ID
		m_IsClientConn = TRUE;			//更改服务器连接状态
	}
}

/*====================================================================
模块名：	客户端请求下载文件列表函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTcInstance::ClientQuery(CMessage *const pMsg)
{

	if (m_IsClientConn == FALSE)
	{
		printf("IS NOT connected\n");
		return;
	}
	//de_bug
		//printf("ClientQuery() was called\n");
	TMsg T_msgQuery;
	memset(&T_msgQuery, 0, sizeof(T_msgQuery));
	memcpy(&T_msgQuery, (TMsg*)pMsg->content, sizeof(T_msgQuery)); //获取用户指令，分为带参和不带参两种
	//de_bug
		//printf("Flag is %d \nMassage is %s \n",T_msgQuery.m_flag,T_msgQuery.m_msg);
	printf("\n%s\n",T_msgQuery.m_msg);	
	//发送查询消息
	::OspPost(m_dwSevInstancId, FT_EVENT_S_FILE_LIST,&T_msgQuery, sizeof(T_msgQuery), m_dwServerNode, MAKEIID(GetAppID(), GetInsID()));
	
}

/*====================================================================
模块名：	客户端处理服务器给出的文件列表函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTcInstance::ClientProcessQueryReply(CMessage *const pMsg)
{
	//de_bug
		printf("%s\n", pMsg->content);
	::OspPrintf(TRUE, FALSE, "%s\n", pMsg->content);  //打印服务器给出的文件列表或错误消息
}

/*====================================================================
模块名：	客户端请求获得文件URL函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTcInstance::ClientReqURL(CMessage *const pMsg)
{
	if (m_IsClientConn == FALSE)
	{
		return;
	}
	s8 ReqUrl[FILEPATH_ABS_LEN];
	strcpy(ReqUrl,(s8*)pMsg->content); 
	//de_bug
		//printf("ReqUrl is %s \n",ReqUrl);
	::OspPost(m_dwSevInstancId, FT_EVENT_S_PRCSS_REQ_DOWNLOAD, ReqUrl, sizeof(ReqUrl), m_dwServerNode, MAKEIID(GetAppID(), GetInsID()));
}

/*====================================================================
模块名：	客户端处理服务器返回的关于URL信息的函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTcInstance::ClientProcessUrlReply(CMessage *const pMsg)
{
	TMsg T_msgUrlRcvd;
	memset(&T_msgUrlRcvd,0,sizeof(T_msgUrlRcvd));
	memcpy(&T_msgUrlRcvd,(TMsg*)pMsg->content,sizeof(T_msgUrlRcvd));
	//如果请求的文件不存在或者请求的文件是一个文件夹，返回相应的错误信息或者该文件夹下的文件列表
	if (T_msgUrlRcvd.m_flag == FALSE)
	{
		printf("\nYour req maybe incorrect \n");
		printf("%s\n", T_msgUrlRcvd.m_msg);
		::OspPrintf(TRUE, FALSE, "%s\n", T_msgUrlRcvd.m_msg);  //打印服务器给出的文件列表或错误消息
	}
	//获得请求文件的URL
	else
	{
		s8 Url[4000];
		memset(Url,0,sizeof(Url));
		strcpy(Url, (s8*)T_msgUrlRcvd.m_msg);
		//de_bug
			//printf("Url = %s\n",Url);
		//获得文件URL，调用下载函数
		::OspPost(MAKEIID(GetAppID(), GetInsID()), FT_EVENT_C_DOWNLOAD, Url, sizeof(Url));
	}
}

/*====================================================================
模块名：	客户端处理服务器返回的关于URL信息的函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTcInstance::ClientDownload(CMessage *const pMsg)
{
	//printf("ClientDownload was called\n");
	s8 fullpath[FILEPATH_ABS_LEN];//要下载文件本地完整路径
	GetFileNameFromUrl((s8*)pMsg->content, fullpath);// 从URL获取下载文件名称并得到本地路径
	//de_bug
		//printf("Fullpath is %s \n",fullpath);
	s64 OffSet = IsFileExist(fullpath);//用于向http服务器发送断点传送信息（在http头部）
	//de_bug
		//printf("OffSet is %d \n",OffSet);
	//下载文件
	u16 flag;
	flag = CurlDownload((s8*)pMsg->content, fullpath, OffSet);
	if (flag != 0)
	{
		::OspPrintf(TRUE, FALSE, "文件下载失败！\n");
		//de_bug
		printf("File download failed\n");
	}
	else
	{
		::OspPrintf(TRUE, FALSE, "文件下载成功，路径为：%s\n",fullpath);
		//de_bug
		printf("[asynchronous message]  : A file download success,Storage path is: %s\n",fullpath);
	}	
}


/*====================================================================
模块名：	与服务器断开连接
返回值：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/14
====================================================================*/
void FTcInstance::ClientDisconn(CMessage *const pMsg)
{
	//向服务器发送断开连接事件
	printf("Disconnecting\n");
	//::OspPost(m_dwSevInstancId, OSP_DISCONNECT, NULL, 0, m_dwServerNode, MAKEIID(GetAppID(), GetInsID()));
	::OspPrintf(TRUE, FALSE, "与服务器断开连接!\n");
}

/*====================================================================
模块名：	获取要下载文件的本地全路径
返回值：	
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/14
====================================================================*/
void GetFileNameFromUrl(s8* URL,s8*fullpath)
{
	//从URL中获取文件名，用于查询本地是否已经下载部分文件
	u16 i = strlen(URL) - 1;
	while (*(URL+i) != '/')  //用于Linux
	{
		--i;
	}
	++i;
	s8 filename[FILENAME_MAX_LEN];
	u16 j = 0;
	memset(filename, 0, sizeof(filename));
	//从URL中获取下载文件的名称（也可从本地请求获取，都一样的）
	while ((*(URL + i) != '\0') && (j < FILENAME_MAX_LEN))
	{
		filename[j++] = *(URL + i++);
	}
	memset(fullpath, 0, sizeof(fullpath));
	strcpy(fullpath, LclStorgePath);
	strcat(fullpath, filename);
}

/*====================================================================
模块名：	查询文件是否存在
返回值：	返回0代表此文件不存在或者偏移量为0，若存在部分，则返回实际的偏移量
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/14
====================================================================*/
s64 IsFileExist(s8* filepath)
{
	FILE* filefd = NULL;
	if ((filefd = fopen(filepath, "r")) == NULL)
	{
		::OspPrintf(TRUE, FALSE, "本地文件不存在!\n");
		return 0;
	}
	s32 fseek_ret = fseeko(filefd, 0, SEEK_END);
	if (fseek_ret == -1)
	{
		perror("fseek error!\n");
	}
	off_t FileOffset = ftello(filefd);  //获取文件偏移量当前
	fclose(filefd);
	::O视频、(TRUE, FALSE, "本地存在该文件的部分!\n");
	return FileOffset;
}

/*====================================================================
模块名：	Liburl下载函数（仅支持http以get方式下载）
返回值：	
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/14
====================================================================*/
BOOL32 CheckDirExist(s8* dirpath);//函数声明：检查本地下载文件夹是否存在，不存在，则创建
/*  libcurl write 回调函数 */
size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written = fwrite(ptr, size, nmemb, stream);
	return written;
}
s32 CurlDownload(const s8* url, const s8* filepath,s64 OffSet) {
	//首先检查下载文件夹是否存在可访问
	if(CheckDirExist(LclStorgePath) == FALSE)
	{
		return -1;
	}
	CURL *curl;
	FILE *fp;
	CURLcode res;
	/*   调用curl_global_init()初始化libcurl  */
	res = curl_global_init(CURL_GLOBAL_ALL);
	if (CURLE_OK != res)
	{
		printf("init libcurl failed.");
		curl_global_cleanup();
		return -1;
	}
	/*  调用curl_easy_init()函数得到 easy u16erface型指针  */
	curl = curl_easy_init();
	if (curl) {
		if (OffSet == 0)
		{
			fp = fopen(filepath, "wb");
			if (fp == NULL)
				perror("open the exist file failed!\n");
		}
		else
		{
			fp = fopen(filepath, "ab");
			if (fp == NULL)
				perror("open file failed!\n");
		}
		/*  调用curl_easy_setopt()设置传输选项 */
		
		res = curl_easy_setopt(curl, CURLOPT_URL, url);

		if (res != CURLE_OK)
		{
			fclose(fp);
			curl_easy_cleanup(curl);
			return -1;
		}
		/*  根据curl_easy_setopt()设置的传输选项，实现回调函数以完成用户特定任务  */
		res = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		if (res != CURLE_OK)
		{
			fclose(fp);
			curl_easy_cleanup(curl);
			return -1;
		}
		/*  根据curl_easy_setopt()设置的传输选项，实现回调函数以完成用户特定任务  */
		res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		if (res != CURLE_OK)
		{
			fclose(fp);
			curl_easy_cleanup(curl);
			return -1;
		}
		//如果本地已存在部分文件，则需要断点传续
		if (OffSet != 0)
		{
			s8 Range[30]; //存储断点下载范围
			sprintf(Range,"%ld",OffSet);
			strcat(Range,"-");
			curl_easy_setopt(curl, CURLOPT_RANGE, Range);
		}

		res = curl_easy_perform(curl);                               // 调用curl_easy_perform()函数完成传输任务  
		fclose(fp);
		/* Check for errors */
		if (res != CURLE_OK) {
			fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
			curl_easy_cleanup(curl);
			return -1;
		}

		/* always cleanup */
		curl_easy_cleanup(curl);                                     // 调用curl_easy_cleanup()释放内存   

	}
	curl_global_cleanup();
	return 0;
}

//函数声明：检查本地下载文件夹是否存在，不存在，则创建
BOOL32 CheckDirExist(s8* dirpath)
{
	if(access(dirpath,F_OK) == 0)
		return TRUE;
	else
	{
		umask(0);
		if(mkdir(dirpath, S_IRWXU | S_IRWXG | S_IRWXO) == 0 )
			return TRUE;
		else
		{
			printf("Fail to create directory, downloading terminated\n");
			return FALSE;
		}
	}
}

//从IP和端口组成的字符串中分离出IP和端口
void GetIpPort(s8 * IPORT,s8* IP,u16* port)
{
	u16 sizeOFbuf = strlen(IPORT);
	u16 i = sizeOFbuf;
	while (i--)
	{
		if (*(IPORT + i) == ':')
		{
			break;
		}
	}
	// tmp[16];
	strncpy(IP,IPORT,i);
	i++;
	s8 port_c[5];
	u16 j = 0;
	for(;i<=sizeOFbuf;i++)
	{
		port_c[j++] = *(IPORT+i);
	}
	u16 port_i = atoi(port_c);
	//printf("port is %d \n",port_i);
}
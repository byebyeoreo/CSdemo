#pragma once
#ifndef FT_CLIENT_H_
#define FT_CLIENT_H_

#include "common.h"
#include <curl/curl.h>



//定义FTcInstance类,继承自CInstance
class FTcInstance : public CInstance
{
public:
	FTcInstance();
	~FTcInstance();

	//消息处理函数
	void InstanceEntry(CMessage *const pMsg);
	void DaemonInstanceEntry(CMessage *const pMsg, CApp *pcApp);


private:
	void ClientInitRequest(CMessage *const pMsg);		//文件传输客户端初始化
	void ClientProcessSevReply(CMessage *const pMsg);	//处理服务器同意连接发来的指令
	void ClientQuery(CMessage *const pMsg);				//查询服务器文件命令
	void ClientProcessQueryReply(CMessage *const pMsg);	//客户端处理查询结果
	void ClientReqURL(CMessage *const pMsg);			//请求下载文件
	void ClientProcessUrlReply(CMessage *const pMsg);  //客户端处理服务器关于下载文件请求的回复
	void ClientDownload(CMessage *const pMsg);				//下载文件
//	void ClientRetry(CMessage *const pMsg);					//重新下载文件
//	void ClientReconnect(CMessage *const pMsg);				//重新连接服务器
	void ClientDisconn(CMessage *const pMsg);				//退出
	
	void ClientTest(CMessage *const pMsg);			//客户端调试
private:
	u32 m_dwServerNode;									//服务器Node号
	u32 m_dwSevInstancId;								//服务器INS的ID
	BOOL32 m_IsClientConn;								//客户端状态
	//s8 m_filename[FILENAME_MAX_LEN];					//函数见传递的下载文件名
};

typedef zTemplate<FTcInstance, FT_CLIENT_INS_NUM, CAppNoData, 20> FTClientApp;
#endif
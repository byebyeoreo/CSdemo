#include "clientdoewn.h"
#inlcude "client.h"

//发送链接请求，IP地址和端口在clientdown.h头文件中宏定义
void CreateConn(char* ip_port)
{
    OspPost(MAKEIID(FT_CLIENT_APP_ID, CInstance::PENDING), FT_EVENT_C2S, ip_port, sizeof(ip_port));
}

//从终端读取你所希望获得的文件夹列表的路径，回车默认返回存储根目录文件列表
void GetFileNameList(char*FileList)
{
    if(strlen(GetFileNameList) == 0)
    {//接收空格
        ::OspPost(MAKEIID(FT_CLIENT_APP_ID, FT_CLIENT_INS_NUM), FT_EVENT_C_INQUIRE_LIST, NULL, 0);
    }
    else
    {//标准输入接收到要查询的文件夹名称
        ::OspPost(MAKEIID(FT_CLIENT_APP_ID, FT_CLIENT_INS_NUM), FT_EVENT_C_INQUIRE_LIST, FileList, sizeof(FileList));
    }
}

//发送下载命令，需要从终端读入要下载文件的相对路径（可以根据GetFileNameList()函数获得路径），
//  storgepath参数根据 clientdown.h头文件中设定填入，表示本地存储文件夹
void SendDownldCmd(char*FileList, char*storgepath)
{
    u16 sendsize = 2* sizeof(FileList);
	s8 sendbuf[sendsize];
	::OspPost(MAKEIID(FT_CLIENT_APP_ID, FT_CLIENT_INS_NUM), FT_EVENT_C_REQ_URL, ReqFileName, sizeof(ReqFileName));
}
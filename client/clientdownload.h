#ifndef _CLIENTDOWNLOAD_H_
#define _CLIENTDOWNLOAD_H_
#define IP_PORT     "127.0.0.1:80" //Change it when you need to customize
#define StorgePath  "/home/downloads/" //Change it when you need to customize. End with '/'
extern "C" void CreateConn(char* ip_port);
extern "C" void GetFileNameList(char*FileList);
extern "C" void SendDownldCmd(char*FileList, char*storgepath);

#endif

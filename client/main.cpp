#include"client.h"

//函数声明
void func(); //用户操作函数
void CreateConn();
void GetFileNameList();
void SendDownldCmd();
int main(int argc, char* argv[])
{
	FTClientApp g_FTClientApp;
	if (TRUE == OspInit(TRUE, 2501))
	//de_bug
		//printf("Init success!\n");//OSP初始化
	OspCreateTcpNode(0, FT_CLIENT_PORT);	//创建监听节点
	g_FTClientApp.CreateApp("FTClientApp", FT_CLIENT_APP_ID, FT_CLIENT_APP_PRIO, FT_APP_QUE_SIZE);
	CreateConn();
	sleep(1);
	func();
	OspQuit();								// 退出Osp
	return 0;
}


//程序运行函数，主要包含读取输入的查询文件名和下载文件名并发送相应命令
void func()
{
	u16 i;
	for(;;)
	{
		sleep(1);	//并非必要，因为客户端是异步发送消息，所以此举是为了等待终端显示服务器回复消息
		printf("------------------------------------------------\n");
		//printf("客户端提供以下4种功能：\n 1. 查询服务器文件列表 | 2. 下载文件 | 3.退出OSP 	输入编号以进入相应功能：\n");
		printf("The client provide 3 function:\n [a]: query the file list | [b]: download | [q]:	quit OSP\nEnter your choice:\n");
		while(1)
		{	
			setbuf(stdin,NULL);// ?????????
			if(!scanf("%d",&i))
				printf("Incorrect input parameter\n");//getchar();
			else
				break;
		}
			if(i == 1)
			{
				GetFileNameList();
				//printf("get filelist\n");
			}
			else if(i == 2)
			{
				printf("Please enter the file you want to download (Example: aa/bb.txt):\n");
				SendDownldCmd();
			}
			else if(i == 3)
			{
				printf("quiting...\n");
				::OspPost(MAKEIID(FT_CLIENT_APP_ID,FT_CLIENT_INS_NUM), OSP_DISCONNECT,NULL, 0);
				sleep(1);
				printf("quit success!\n");
					
				break;
			}
			else
			{
				printf("Incorrect input parameter(enter a,b,q)\n");
			}

	}
}

//请求服务器建立链接
void CreateConn()
{	
	while (1)
    {
		setbuf(stdin,NULL);// 清空标准输入
		printf("\nInput character 'c' to connect the server\n");

		if(getchar() == 'c')
		{
			OspPost(MAKEIID(FT_CLIENT_APP_ID, CInstance::PENDING), FT_EVENT_C2S, NULL, 0);                  //连接服务器
			break;
		}
    } 
}

//从终端获得要获得的文件列表
void GetFileNameList()
{
	printf("Enter the file path that you want to get,(if emtpy, Server will retrun a list of files in the storage root directory)\n ");
	printf("If you don't know the file path,we suggest that you should press Enter \n");
	s8 filepath[StructMassageSize];
	memset(filepath,0,StructMassageSize);
	//printf("===================================================================\n");
	setbuf(stdin,NULL);// 清空标准输入
	//setbuf(stdout,NULL);
	gets(filepath);
	//de_bug
		//printf("filepath is %s\n",filepath);
	TMsg T_msgQuerySend;
	if(strlen(filepath) == 0)
	{
		//无参查询
		
		T_msgQuerySend.m_flag = FALSE;
		memset(T_msgQuerySend.m_msg,0,sizeof(T_msgQuerySend.m_msg));
		::OspPost(MAKEIID(FT_CLIENT_APP_ID, FT_CLIENT_INS_NUM), FT_EVENT_C_INQUIRE_LIST, &T_msgQuerySend, sizeof(T_msgQuerySend));
	}
	else
	{
		T_msgQuerySend.m_flag = TRUE;
		strcpy(T_msgQuerySend.m_msg,filepath);
		//printf("T_msgQuerySend.m_msg is %s\n",T_msgQuerySend.m_msg);
		::OspPost(MAKEIID(FT_CLIENT_APP_ID, FT_CLIENT_INS_NUM), FT_EVENT_C_INQUIRE_LIST, &T_msgQuerySend, sizeof(T_msgQuerySend));
		//de_bug
		//::OspPost(MAKEIID(FT_CLIENT_APP_ID, FT_CLIENT_INS_NUM), FT_EVENT_C_INQUIRE_LIST, "hello", sizeof("hello"));
	}
}

//发送下载文件命令
void SendDownldCmd()
{
	s8 ReqFileName[FILEPATH_ABS_LEN];
	setbuf(stdin,NULL);// 清空标准输入
	setbuf(stdout,NULL);
	gets(ReqFileName);
	if(strlen(ReqFileName) == 0)
	{
		printf("Your input is empty\n");
		return;
	}
	//de_bug
	//printf("ReqFileName is %s\n",ReqFileName);
	::OspPost(MAKEIID(FT_CLIENT_APP_ID, FT_CLIENT_INS_NUM), FT_EVENT_C_REQ_URL, ReqFileName, sizeof(ReqFileName));
}
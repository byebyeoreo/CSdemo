#include<sqlite3.h>
#include "ip_check.h"

u16 is_exist = 0; //此变量仅用于检测IP名单是否存在以及查询语句
int callback(void * NotUsed, int argc, char ** argv, char ** azColName)
{
	is_exist = *argv[0] - '0';
	return 0;
}
#include"ip_check.h"
/*sqlite3 回调函数*/

//打开IP保存名单的数据库表
BOOL32 table_create()
{
	sqlite3 *db;
	char *zErrMsg = 0; 	int  rc;
	char *sql;
	is_exist = 0;
	/* Open database */
	rc = sqlite3_open(IP_DB, &db);
	if (rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		return FALSE;
	}
	else {
		//fprintf(stdout, "Opened database successfully\n");
	}
	//查询IP名单表是否存在
	sql = "SELECT count(*) FROM sqlite_master WHERE type='table' AND name='ip_table'"; //本处并没有采用参数化SQL语句，如需要，可更改
	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

		if (is_exist == 1) {
			//de_bug
			//fprintf(stdout, "Table already exists!\n");
			sqlite3_close(db);
			return TRUE;
		}

		else
		{
			sql = "CREATE TABLE ip_table("  \
				"IP INT PRIMARY KEY     NOT NULL," \
				"SCRID           INT    NOT NULL," \
				"IS_MASK        INT		NOT NULL);";
			/*SQL表中定义了客户端IP地址，客户端APP的ID和实例的ID，以及此IP是否被屏蔽*/

			/* Execute SQL statement */
			rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
			if (rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", zErrMsg);
				sqlite3_free(zErrMsg);
			}
			else {
				fprintf(stdout, "Table created successfully\n");
				return TRUE;
			}
		}

}

//针对数据库的增改操作
void table_operation(const char* sql)
{
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	const char* data = "Callback function called";

	/* Open database */
	rc = sqlite3_open(IP_DB, &db);
	if (rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	}
	/*else {
	fprintf(stderr, "Opened database successfully\n");
	}
	*/
	/* Execute SQL statement */
	rc = sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		sqlite3_close(db);
	}
	printf("Operation successful\n");
	sqlite3_close(db);
}


//查询ip是否位于黑名单
BOOL32 is_ip_mask(s32 ip)
{	
	//de_bug
	//printf("ip is %d\n",ip);
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	char *sql;
	sqlite3_stmt *res;
	/* Open database */
	rc = sqlite3_open(IP_DB, &db);
	if (rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		return TRUE;
	}
	else {
		//fprintf(stderr, "IS_MASK:Opened database successfully\n");
	}
	/* 创建SQL语句 */
	
		
/*is_exist = 0;
		sprintf(sql,"SELECT count(*) from ip_table WHERE IP = '%d' AND IS_MASK = '1';",ip);
		rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

		if (is_exist == 1) {
			fprintf(stdout, "IP is masked!\n");
			sqlite3_close(db);
			return TRUE;
		}
		else
		{
			printf("IP is not masked\n");
			return FALSE;
		}*/
		
	sql = "SELECT count(*) from ip_table WHERE IP = ? AND IS_MASK = '1'";
	/* 执行SQL语句 */
	rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
	if (rc == SQLITE_OK)
	{
		sqlite3_bind_int(res, 1, ip);
		//printf("bind success!\n");
	}
	else {
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
		return TRUE;
	}
	int step = sqlite3_step(res);
	if (step == SQLITE_ROW) {
		//printf("%d \n", sqlite3_column_int(res, 0));
		u16 IsMsak = sqlite3_column_int(res, 0);
		
		sqlite3_finalize(res);
		sqlite3_close(db);
		if(IsMsak == 1)
			return TRUE;
		else
			return FALSE;
	}
	
}

BOOL32 is_ip_record(u32 ip)
{
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	char *sql;
	sqlite3_stmt *res;
	/* Open database */
	rc = sqlite3_open(IP_DB, &db);
	if (rc) {
		fprintf(stderr, "Can't open database: %s when Check whether the IP is exist\n", sqlite3_errmsg(db));
		return FALSE;
	}
	else {
		//fprintf(stderr, "Opened database successfully\n");
	}
	/* 创建SQL语句 */
	sql = "SELECT count(*) from ip_table WHERE IP = ?;";

	/* 执行SQL语句 */
	rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
	if (rc == SQLITE_OK)
	{
		sqlite3_bind_int(res, 1, ip);
		//printf("bind success!\n");
	}
	else {
		fprintf(stderr, "Failed to execute statement: %s\n", sqlite3_errmsg(db));
		return FALSE;
	}
	int step = sqlite3_step(res);
	if (step == SQLITE_ROW) {
		//de_bug
		//printf("sqlite3_column_int(res, 0) = %d \n", sqlite3_column_int(res, 0));
		u16 IsRecord = sqlite3_column_int(res, 0);
		sqlite3_finalize(res);
		sqlite3_close(db);
		if(IsRecord == 1)
			return TRUE;
		else
			return FALSE;
	}

}

void table_on_transmit(s32 * arr)
{	
	int i = 0;
	sqlite3 *db;
	char *zErrMsg = 0;
	int rc;
	char *sql;
	sqlite3_stmt *res;
	/* Open database */
	rc = sqlite3_open("IP_info.db", &db);
	if (rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	}
	else {
		//fprintf(stderr, "Opened database successfully\n");
	}
	/* 创建SQL语句 */
	sql = "SELECT ID from ip_table WHERE IS_TRANMISTTING = '1'";

	/* 执行SQL语句 */
	rc = sqlite3_prepare_v2(db, sql, -1, &res, 0);
	if (rc == SQLITE_OK)
	{
		printf("-------------\n");
		while (sqlite3_step(res) == SQLITE_ROW) {
			*(arr + i) = sqlite3_column_int(res, 0);
		}
		sqlite3_finalize(res);
		sqlite3_close(db);
	}
	
}

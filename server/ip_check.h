#ifndef IP_CHECK_H_
#define IP_CHECK_H_
#pragma once
#include<sqlite3.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"kdvtype.h"

#define IP_DB "ip_info.db"  //定义客户端IP信息名单存储的数据库名称
#define IP_TABLE "ip_table"		//定义客户端IP信息存储的表

//extern u16 is_exist; //此变量仅用于检测IP名单是否存在以及查询语句
static int callback(void *NotUsed, int argc, char **argv, char **azColName); //回调函数
BOOL32 table_create();
void table_operation(const char * sql);
BOOL32 is_ip_mask(s32 ip);
BOOL32 is_ip_record(u32 ip);
void table_on_transmit(s32 * arr);
#endif

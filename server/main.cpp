#include "server.h"
#include "mongoose.h"

typedef zTemplate<FTsInstance, FT_SERVER_MAX_INS> FTServerApp;
//设置http服务器选项
static struct mg_serve_http_opts s_http_server_opts;
static const s8 *s_http_port = Http_Port;										//设置http服务器的服务端口
//http 处理函数
static void ev_handler(struct mg_connection *nc, int ev, void *p) {
  if (ev == MG_EV_HTTP_REQUEST) {
    mg_serve_http(nc, (struct http_message *) p, s_http_server_opts);
  }
}


int main(int argc, char *argv[])
{
		//创建http服务
	printf("Begining create http service\n");
	struct mg_mgr mgr;
	struct mg_connection *nc;
	mg_mgr_init(&mgr, NULL);
	printf("Starting web server on port %s\n", s_http_port);
	nc = mg_bind(&mgr, s_http_port, ev_handler);
	if (nc == NULL) 
	{
		printf("Failed to create listener\n");
		return 1;
	}
	
	// Set up HTTP server parameters
	mg_set_protocol_http_websocket(nc);
	s_http_server_opts.document_root = Server_path;
	s_http_server_opts.enable_directory_listing = "yes";
	printf("http server enabled\n");
	
	//创建OSP的APP服务
	FTServerApp g_FTServerApp;
	if (TRUE == OspInit(TRUE, 2500))
		printf("Init success!\n");//OSP初始化
	OspCreateTcpNode(0, FT_SERVER_PORT);	//创建监听节点
	g_FTServerApp.CreateApp("FTServerApp",FT_SERVER_APP_ID,FT_SERVER_APP_PRIO,FT_APP_QUE_SIZE);


	while (1)
	{

		mg_mgr_poll(&mgr, 1000);
	}

	OspQuit();								// 退出Osp

	return 0;
	//printf("hello\n");
}

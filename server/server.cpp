#include"server.h"

u16 g_connecting_client = 0;
vector<u32> g_node(FT_SERVER_MAX_INS,0);

BOOL32  GetFileList(const s8* path, s8* filelist); //函数声明

/*====================================================================
模块名：	构造函数
实现功能：
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
FTsInstance::FTsInstance() {
	m_isconnect = FALSE;
	m_istransmitting = FALSE;
}


/*====================================================================
模块名：	析构函数
实现功能：	
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
FTsInstance::~FTsInstance() {

}


/*====================================================================
模块名：	守护实例处理函数
实现功能：	处理客户端新连接和OSP断开
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
	日 期		版本	修改人		走读人		修改记录
  2017/06/09
====================================================================*/

void FTsInstance::DaemonInstanceEntry(CMessage *const pMsg, CApp *pcApp)
{

	u16 wCurEvent = pMsg->event;
	switch (wCurEvent)
	{
	
	case OSP_DISCONNECT:				//	osp	断开链接
		SevProcessDisconn(pMsg);
		//NextState(IDLE_STATE);
	default:
		OspPrintf(TRUE, FALSE, "\n错误, 非法事件类型！\n");
		break;
	}
}


/*====================================================================
模块名：	普通守护实例处理函数
实现功能：	处理客户端一般请求
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTsInstance::InstanceEntry(CMessage *const pMsg)
{
	u16 wCurEvent = pMsg->event;
	switch (wCurEvent)
	{
	case FT_EVENT_S_PRCSS_REQ:			//处理客户端连接请求
		SevProcessReq(pMsg);
		NextState(RUNNING_STATE);
		break;
	case FT_EVENT_S_FILE_LIST:			//列出可供下载的文件
		SevListFile(pMsg);
		break;
	case FT_EVENT_S_PRCSS_REQ_DOWNLOAD:		//处理客户端下载请求
		SevProcessReqDownload(pMsg);
		break;
	case OSP_DISCONNECT:				//	osp	断开链接
		SevProcessDisconn(pMsg);
		break;
//	case FT_EVENT_S_DOWNLOADING:		//列出正在下载文件的客户端信息
//		SevListDownldingClient(pMsg);
//		break;
	default:
		OspPrintf(TRUE, FALSE, "\n错误, 非法事件类型！\n");
		break;
	}
}


/*====================================================================
模块名：	处理客户端连接申请函数
实现功能：	服务器处理客户端新连接，服务器查询客户端IP是否在IP黑名单，服务器
			检查是否有空闲实例。若不在黑名单且有空闲实例，接受连接请求，
			并设置断链检测函数，检查IP是否在IP列表中，没有则添加；否则，
			拒接连接。
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTsInstance::SevProcessReq(CMessage *const pMsg)
{
	//de_bug
	printf("--------------------A requirring is here----------------------\n");
	//char buf[1000];
	//memset(buf,'z',1000);
	//::OspPost(pMsg->srcid, FT_EVENT_C_TEST, buf, sizeof(buf), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
	if (g_connecting_client < FT_SERVER_MAX_INS)
	{
		//
		//de_bug
		//::OspPrintf(TRUE, FALSE,"ip o is ;%d \n",OspNodeIpGet(pMsg->srcnode));
		u32 srcip = OspNodeIpGet(pMsg->srcnode);		//获得客户端IP
		
		if(TRUE == table_create())
			::OspPrintf(TRUE, FALSE, "IP table is exist!\n");
		if (is_ip_mask(srcip))  //如果位于黑名单

		{
			::OspPrintf(TRUE, FALSE, "客户端位于黑名单中 !\n");
			//定义消息体
			TMsg T_msg_reject;
			strcpy(T_msg_reject.m_msg, "Your client's IP is in black list !");
			T_msg_reject.m_flag = FALSE;
			
			//de_bug
			printf("This IP is masked\n");
			//printf("Sending a massage of refuse\n");
			
			::OspPost(pMsg->srcid, FT_EVENT_C_PRCSS_RPLY_C2S, &T_msg_reject, sizeof(T_msg_reject), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
			::OspPost(MAKEIID(GetAppID(), GetInsID()), OSP_DISCONNECT, NULL, 0);  //如果在IP黑名单，调用断开函数
		}
		else
		{
			/*设置断链检测函数*/
			if (!::OspSetHBParam(pMsg->srcnode, DisCon_Detection_Timeout, DisConn_Detction_Num))
			{
				::OspPrintf(TRUE, FALSE, "Error,failed to Set OspSetHBParam!\n");
				perror("OspSetHBParam\n");
			}
			/*增加断链通知列表*/
			if (::OspNodeDiscCBRegQ(pMsg->srcnode, GetAppID(), GetInsID()) != OSP_OK)
			{
				::OspPrintf(TRUE, FALSE, "Error,failed to Set OspNodeDiscCBRegQ!\n");
				perror("OspNodeDiscCBRegQ\n");
			}
			TMsg T_msg_accept;
			strcpy(T_msg_accept.m_msg, "You've connected the server!\n");
			T_msg_accept.m_flag = TRUE;
			::OspPost(pMsg->srcid, FT_EVENT_C_PRCSS_RPLY_C2S, &T_msg_accept, sizeof(T_msg_accept), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
			if(m_isconnect == FALSE)
				g_connecting_client++;  //r如果未连接数目增加1，已连接则不增加
			//printf("g_connecting_client is %d \n",g_connecting_client);
			m_isconnect = TRUE;		//设置此INS为连接状态
			if (!is_ip_record(srcip))
			{				
				//如果没有在IP表中，在sqlite数据表中添加一个记录
				s8 sql[256];// 安全起见，下面使用了snprintf函数而非sprintf，防止内存越界
				snprintf(sql,256,"INSERT INTO ip_table (IP,SCRID,IS_MASK) VALUES ('%d','%d','0');", srcip, pMsg->srcid);
				table_operation(sql);  //添加IP记录
			}
			//de_bug
			printf("A new client is connect,is %d-th device!\n",g_connecting_client);
		}
	}
	else
	{
		TMsg T_msg_reject;
		strcpy(T_msg_reject.m_msg, "No more resource for your client!\n");
		T_msg_reject.m_flag = FALSE;
		::OspPost(pMsg->srcid, FT_EVENT_C_PRCSS_RPLY_C2S,&T_msg_reject, sizeof(T_msg_reject), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
		::OspPost(MAKEIID(GetAppID(), GetInsID()), OSP_DISCONNECT, NULL, 0);  //如果服务器连接客户端达到最大，调用断开函数
	}
}




/*====================================================================
模块名：	服务器列出可下载文件函数
实现功能：	服务器列出可下载的文件信息，为避免文件路径过深，服务器只给出
			4层深的文件信息
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTsInstance::SevListFile(CMessage *const pMsg)
{
	if (m_isconnect == FALSE)
	{
		printf("INS is not running \n");
		return;
	}
	//printf("client require the file list\n");
	TMsg query;
	memset(&query, 0, sizeof(query));
	memcpy(&query, (TMsg*)pMsg->content, sizeof(query));
	//de_bug
		printf("query.m_msg is %s\nquery.m_flag is %d\n",query.m_msg,query.m_flag);
	s8 filelist[40960];
	memset(filelist,0,sizeof(filelist));
	//客户端不带参数查询(参数是存储路径下文件夹)，亦即查询存储根目录文件列表
	if (query.m_flag == FALSE)
	{
		if (TRUE == GetFileList(Server_path, filelist))
		{
			//de_bug
				//printf("filelist is %s \n",filelist);
			::OspPost(pMsg->srcid, FT_EVENT_C_INQUIRE_ACK_RCV, filelist, sizeof(filelist), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
		}
		else
		{
			s8 *errmsg = "查询存储文件失误！";
			::OspPost(pMsg->srcid, FT_EVENT_C_INQUIRE_ACK_RCV, errmsg, sizeof(errmsg), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
		}
	}
	/*带参数查询*/
	else
	{
		s8 fullpath[FILE_ABSPATH_LEN];
		strcpy(fullpath,Server_path);
		strcat(fullpath, query.m_msg);
		//de_bug
			printf("fullpath is %s \n",fullpath);
		if (TRUE == GetFileList(fullpath, filelist))
		{
			//de_bug
				//printf("GetFileList(Server_path, fullpath) was called \n");
			::OspPost(pMsg->srcid, FT_EVENT_C_INQUIRE_ACK_RCV, filelist, sizeof(filelist), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
		}
		else
		{
			s8 *errmsg = "查询存储文件失误！";
			::OspPost(pMsg->srcid, FT_EVENT_C_INQUIRE_ACK_RCV, errmsg, sizeof(errmsg), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
		}
	}
}

/*====================================================================
模块名：	服务器处理请求下载文件函数
实现功能：	服务器给出请求下载的文件的本地相对路径，也就是URI，若不存
			在所请求的文件或者请求的文件为文件夹，返回相应的错误信息	
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTsInstance::SevProcessReqDownload(CMessage *const pMsg)
{
	if (m_isconnect == FALSE)
	{
		return;
	}
	//de_bug
	printf("SevProcessReqDownload() was called\n");
	s8 fullpath[FILE_ABSPATH_LEN];
	strcpy(fullpath, Server_path);
	strcat(fullpath, (const s8*)pMsg->content); /*获取请求文件相对路径*/
	//de_bug
		printf("Requirring the %s \n",fullpath);
	TMsg T_replydownload;
	memset(&T_replydownload, 0, sizeof(T_replydownload));
	//判断文件是否存在
	if(access(fullpath,F_OK) != 0)
	{
		T_replydownload.m_flag = FALSE;
		strcpy(T_replydownload.m_msg,"ERROR	:	The requested file does not exist\n");
		::OspPost(pMsg->srcid, FT_EVENT_C_PRCSS_RPLY_URL, &T_replydownload, sizeof(T_replydownload), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
		return ;
	}
	//判断是否为文件夹
	struct stat filestat;
	stat(fullpath,&filestat);
	if (S_ISDIR(filestat.st_mode))
	{
		T_replydownload.m_flag = FALSE;  //设置标志位为FALSE，表明返回的不是请求资源的URL
		s8 filelist[FILELIST_MAXLEN];
		if (TRUE == GetFileList(fullpath, filelist))
		{
			strcpy(T_replydownload.m_msg,filelist);  //返回此文件夹下的一级子文件
			//printf("SEV:	T_replydownload.m_msg is %s\n",T_replydownload.m_msg);
			::OspPost(pMsg->srcid, FT_EVENT_C_PRCSS_RPLY_URL, &T_replydownload, sizeof(T_replydownload), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
		}
		else
		{
			strcpy(T_replydownload.m_msg, "ERROR: 请求文件出错!");  //返回此文件夹下的一级子文件
			::OspPost(pMsg->srcid, FT_EVENT_C_PRCSS_RPLY_URL, (s8 *)&T_replydownload, sizeof(T_replydownload), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
		}
	}
	else
	{
		T_replydownload.m_flag = TRUE;  //设置标志位为TRUE，表明返回的是请求资源的URL
		sprintf(T_replydownload.m_msg,"http://%s:%s/%s", Server_Ip, Http_Port,pMsg->content);
		::OspPost(pMsg->srcid, FT_EVENT_C_PRCSS_RPLY_URL, &T_replydownload, sizeof(T_replydownload), pMsg->srcnode, MAKEIID(GetAppID(), GetInsID()));
	}
	
}

/*====================================================================
模块名：	服务器处理断开连接函数
实现功能：	服务器处理断开连接
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09
====================================================================*/
void FTsInstance::SevProcessDisconn(CMessage *const pMsg)
{
	//de_bug
	printf("server's osp is closing\n");
	m_isconnect = FALSE;
	g_connecting_client--;
//	::OspDisableNodeCheck(pMsg->srcnode);	//关闭该结点的链路检测功能
//	::OspDisconnectTcpNode(pMsg->srcnode);	//关闭与之的TCP连接
}


/*====================================================================
模块名：	目录查询函数函数
实现功能：	
作者 ：		peili hu
--------------------------------------------------------------------------------
修改记录：
日 期		版本	修改人		走读人		修改记录
2017/06/09	V1.0
====================================================================*/
BOOL32  GetFileList(const s8* path, s8* filelist)
{
	DIR *dp;
	struct dirent *entry;
	struct stat filestat;

	if ((dp = opendir(path)) == NULL) {
		::OspPrintf(TRUE, FALSE, "不能打开文件 %s!\n",path);
		return FALSE;
	}
	chdir(path);
	//s8 FileName[FILELIST_MAXLEN] = {0}; //带传送的文件列表
	s8 FileSingle[256]; //单个文件名称大小
	while((entry = readdir(dp)) != NULL)
	{
		lstat(entry->d_name, &filestat);
		if (S_ISDIR(filestat.st_mode)) {
			if (strcmp(entry->d_name, ".") == 0 ||strcmp(entry->d_name, "..") == 0)
				continue;
			sprintf(FileSingle, "DIR	:	%s", entry->d_name);
			strcat(FileSingle, "\r\n");
			strcat(filelist,FileSingle);
		}
		else
		{
			sprintf(FileSingle, "REG	:	%s", entry->d_name);
			strcat(FileSingle, "\r\n");
			strcat(filelist, FileSingle);
		}
	}
	closedir(dp);
	return TRUE;
}


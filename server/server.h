#pragma once
#pragma once
#ifndef FTSERVER_H_
#define FTSERVER_H_

#include "common.h"
#include "ip_check.h"
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include<sys/stat.h>
#include<vector>


//using std::vector;
//using  std::string;
//using std::ios;
#define FILELIST_MAXLEN					40960						//定义传输文件列表的数组大小




//定义FTcInstance类,继承自CInstance
class FTsInstance : public CInstance
{
public:
	FTsInstance();
	~FTsInstance();

	//消息处理函数
	void InstanceEntry(CMessage *const pMsg);
	void DaemonInstanceEntry(CMessage *const pMsg, CApp *pcApp);


private:
	void SevProcessReq(CMessage *const pMsg);			//服务器处理来自客户端的请求
//	void SevSetHeartbeat(CMessage *const pMsg);			//服务器INS设置心跳函数
	void SevListFile(CMessage *const pMsg);				//服务器列出可下载文件
	void SevProcessReqDownload(CMessage *const pMsg);	//服务器处理请求下载文件
//	void SevProcessRetry(CMessage *const pMsg);			//服务器处理续传文件请求(这个功能由客户端调用curl库实现)
//	void SevListDownldingClient(CMessage *const pMsg);	//服务器列出正在下载文件的客户端信息
	void SevProcessDisconn(CMessage *const pMsg);		//服务器处理断开连接


private:
	BOOL32 m_isconnect;
	BOOL32 m_istransmitting;
};
#endif